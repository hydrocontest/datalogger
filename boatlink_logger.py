#!/usr/bin/env python3

# Copyright 2018 Jacques Supcik / HEIA-FR
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

#
# This program subscribes to redis messages, transform them
# and broadcast them to socket.io.
#

import json
import logging
import logging.handlers
import redis
import click

logger = logging.getLogger("boatlink")


@click.command()
@click.option('--debug', is_flag=True)
@click.option('--logfile', 'filename', default='/var/log/boatlink.log')
@click.option('--max-size', 'maxsize', default=4*1024*1024)
@click.option('--backup-count', 'backup_count', default=16)
@click.option('--redis', 'redis_url', default="redis://")
def main(debug, filename, maxsize, backup_count, redis_url):

    formatter = logging.Formatter(
        '%(asctime)s - %(name)s - %(levelname)s - %(message)s')
    ch = logging.StreamHandler()
    ch.setFormatter(formatter)
    logger.addHandler(ch)
    fh = logging.handlers.RotatingFileHandler(
        filename, maxBytes=maxsize, backupCount=backup_count)
    fh.setFormatter(formatter)
    logger.addHandler(fh)

    if debug:
        logger.setLevel(logging.DEBUG)
    else:
        logger.setLevel(logging.INFO)
        ch.setLevel(logging.WARNING)

    r = redis.from_url(redis_url)
    p = r.pubsub()
    p.subscribe('heiafr:hydrocontest:message')
    for message in p.listen():
        if message['type'] == "message":
            m = json.loads(message['data'].decode("utf-8"))
            logger.info(json.dumps(m))


# pylint: disable=no-value-for-parameter
if __name__ == '__main__':
    main()
