#!/bin/bash

rm -Rf dist
source venv/bin/activate
python3 setup.py build
python3 setup.py sdist bdist_wheel
python3 setup.py clean --all
deactivate
